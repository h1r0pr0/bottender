from gpiozero import OutputDevice
from time import sleep

#Pump dictionary 
pump_dict = {}

#Creates pump dictonary
def create_pumps(num_of_pumps, useable_pins):
    while num_of_pumps > 0:
        pin_count = next(useable_pins)
        "  pump"+useable_pins.index(pin_count) = OutputDevice(pin_count, *, active_high=True, initial_value=False, pin_factory=None)
        pump_dict["pump"+useable_pins.index(pin_count)] = "unassigned"
        num_of_pumps -= 1 
    return pump_dict

#Assign pump liquid
def assign_pump(pump_number, liquid_name):
    pump_dict[pump_number] = liquid_name
    return pump_dict

#Select pump by liquid
def path_pump(liquid_name)
    for pump_name in pump_dict:
        if pump_dict[pump_name] == liquid_name:
            return pump_name

#Turn pump on by liquid name
def pump_on(liquid_name)
    pump_number = path_pump(liquid_name)
    pump.on
    return

#Turn pump off by liquid name
def pump_off(liquid_name)
    pump_number = path_pump(liquid_name)
    pump.off
    return

#Edit pump liquid
def change_pump(liquid_name, new_liquid_name)
    pump_number = path_pump(liquid_name)
    pump_dict[pump_number] = liquid_name
    return

#Timed pour
def timed_pour(liquid_name, timer)
    pump_on(liquid_name)
    sleep(timer)
    pump_off(liquid_name)
    return

#Cleaning Cycle
def cleaning_cycle
    for pump in pump_dict:
        pump.on 
        sleep(60)
        pump.off
    return

#Wieght measurement
def liquid_scale(start_wieght, wieght)
    end_wieght = start_wieght + wieght
    while start_wieght < end_wieght:
        print(pouring)
    return

#Pour by wieght
def pour_component(liquid_name, wieght)
    pump_on(liquid_name)
    liquid_scale(wieght)
    pump_off(liquid_name)

#Pour by recipe
def pour_recipe(recipe)
    for component in recipe
        pour_component(component, recipe[component])
    return

#Examples

#useable_pins for pi = (7, 11, 12, 13, 15, 16, 18, 22, 29, 31, 32, 33, 35, 36, 37, 38, 40)

#recipe
mowcow_mule{
    "vodka" : 2
    "ginger beer" : 4
    "lime juice" : 0.5
}

#call for moscow mule after start-up 
create_pumps(3, useable_pins)
assign_pump("pump1", "vodka")
assign_pump("pump2", "ginger beer")
assign_pump("pump3", "lime juice")
pour_recipe(moscow_mule)

